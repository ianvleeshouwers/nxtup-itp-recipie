import Layout from '../../components/layout';
import Link from "next/link";
import Head from 'next/head';
import {RichText} from "prismic-reactjs";
import { Client } from "../../prismic-configuration";

const Recipe = ({pie}) => (
    <div>
        <Head>
            <title>Recipie | {pie.data.title}</title>
        </Head>

        
        <Layout root="../">
            <div className="container">

                {RichText.render(pie.data.headtitle)}
                <div className="top">
                    <div className="banner">
                        <img  src={pie.data.image.url} alt={pie.data.image.alt} />
                    </div>

                    <div className="properties">
                        <ul className="props">
                            <li>
                                <img src="../images/person.svg" alt="person" />
                                {pie.data.properties[0].text}
                            </li>
                            <li>
                                <img src="../images/time.svg" alt="time" />
                                {pie.data.properties[1].text}
                            </li>
                            <li className="category">
                                <Link href={`/${pie.tags[0]}`}>
                                <div>
                                    <img src="../images/category.svg" alt="category" />
                                    {pie.data.properties[2].text}
                                </div>
                                </Link>
                            </li>
                            <li>
                                <img src="../images/cook.svg" alt="cooking time" />
                                {pie.data.properties[3].text}
                            </li>
                        </ul>

                        <ul>
                            {pie.data.ingredientlist.map( (ingredient, index) => (
                                <li className="ingredient" key={index}>
                                    <img src="../images/check.svg" alt="check" />
                                    {ingredient.text}
                                </li>
                            ))}
                        </ul>

                    </div>
                </div>

                <div className="recipe">
                    <h2>Recipe</h2>
                    <div className="description">
                        <p>{pie.data.description[0].text}</p>
                    </div>

                    <ul>
                    {pie.data.recipetext.map( (step, index) => (
                        <li key={index}>{step.text}</li>
                    ))}
                    </ul>
                </div>

                
                
            </div>
        </Layout>
    </div>
);

export default Recipe;



export async function getServerSideProps(context) {
    
    const pie = await Client().getByUID("pie", context.query.id);
    return {
        props : {
            pie: pie
        }
    };
}

