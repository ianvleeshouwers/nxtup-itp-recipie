import Layout from '../components/layout';
import Head from 'next/head';
import Prismic from "prismic-javascript";
import {RichText} from "prismic-reactjs";
import Link from "next/link";
import { Client } from "../prismic-configuration";

const Desserts = ({ pies }) => (
    <div>
        <Head>
            <title>Desserts</title>
        </Head>

        
        <Layout root="">
            <div>
                <h1>Desserts</h1>
                {pies.results.map( (pie, index) => (
                    <div key={index} className="overviewItem">
                        
                        <Link href={`/recipe/${pie.uid}`}>
                            <div>
                                <div className="image">
                                    <img src={pie.data.image.url} alt={pie.data.image.alt} />
                                </div>
                                {RichText.render (pie.data.subtitle)}
                            </div>
                            
                        </Link>
                    </div>
                    
                    
                ))}
            </div>
        </Layout>
    </div>
);

export default Desserts;



export async function getServerSideProps() {
    const pies = await Client().query(
        Prismic.Predicates.at("document.tags", ["dessert"])
    );

    return {
        props : {
            pies: pies
        }
    };
}

