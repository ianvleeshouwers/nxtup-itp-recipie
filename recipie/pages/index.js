import Head from 'next/head';
import Layout from '../components/layout';
import Prismic from "prismic-javascript";
import {RichText} from "prismic-reactjs";
import Link from "next/link";
import { Client } from "../prismic-configuration";

export default function Home({pies}) {
  return (
    <div>
      <Head>
        <title>Recipies | overview</title>
        
      </Head>

      <Layout root="">
        <main>
          {pies.results.map( (pie, index) => (
            <div key={index} className="overviewItem">
              <Link href={`/recipe/${pie.uid}`}>
                <div>
                  <div className="category">
                    <Link href={`/${pie.tags[0]}`}>
                      {pie.tags[0]}
                    </Link>
                  </div>
                  <div className="image">
                    <img src={pie.data.image.url} alt={pie.data.image.alt} />
                  </div>
                                
                  {RichText.render (pie.data.subtitle)}
                </div>
              </Link>
            </div>
          ))}
        </main>
      </Layout>
      </div>
  )
}

// GraphQL alternative 
export async function getServerSideProps() {
  const pies = await Client().query(
      Prismic.Predicates.at("document.type", "pie")
  );

  console.log(pies)

  return {
      props : {
          pies: pies
      }
  };
}
