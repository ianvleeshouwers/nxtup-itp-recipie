import Head from 'next/head';
import Link from 'next/link';

const Layout = ({children, root}) => (
    <div>
        <Head>
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet"></link>
            <link rel="icon" href="/favicon.ico" />
        </Head>
        
        <header>
            <div className="container">
                <ul>
                    <li>
                        <Link href="/">
                            <img className="logo" src={`${root}images/logo.svg`} />
                        </Link>
                    </li>
                </ul>
            </div>
            

            <img className="logoTxt" src={`${root}images/logo-txt.svg`} alt="recipies" />
            
        </header>

        <main>
            { children }
        </main>
        

        <footer>
            Created by Ian Vleeshouwers
        </footer>
    </div>
);

export default Layout;